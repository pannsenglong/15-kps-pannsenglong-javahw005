public class Loading extends Thread
{
    public String text;
    public int delayTime;

    public Loading(String text, int delayTime) {
        this.text = text;
        this.delayTime = delayTime;
    }

    @Override
    public void run() {
        loadingPrint(this.text, this.delayTime);
    }

    void loadingPrint(String text, int delayTime){
        for (int i=0; i<text.length(); i++)
        {
            System.out.print(text.charAt(i));
            try {
                Thread.sleep(delayTime);
            }catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
    public static void main(String args[]) {
        String text = "Hello HRD!\n"
                     +"**************************************\n"
                     +"I will try my best to be here at HRD.\n"
                     +"--------------------------------------\n";
        Loading loading1 = new Loading(text,200);
        Loading loading2 = new Loading("Download",0);
        Loading loading3 = new Loading("..........  ",200);
        Loading loading4 = new Loading("Loading 100%", 0);
        try {
            loading1.start();
            loading1.join();
            loading2.start();
            loading2.join();
            loading3.start();
            loading3.join();
            loading4.start();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}

